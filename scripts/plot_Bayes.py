import numpy as np
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from scipy.stats import norm, gaussian_kde
import sys

B = []
e = []
filename = str(sys.argv[1]) # provides data produced by NestleNestedSampling.py
data = np.genfromtxt(filename) # retrieves data produced by NestleNestedSampling.py
#fh = open(filename,'r') 
#for line in fh.readlines():
#    B.append(line.split()[0])
#    e.append(line.split()[1])
#B = np.array(B).astype('float')
#e = np.array(e).astype('float')

B = np.log10(data[:,0]) # log(Bayes Factor)
e = np.log10(data[:,1]) # p(log(Bayes Factor)) # probability

print(B) 

kernel = gaussian_kde(B) # kernel density estimate

xvec = np.linspace(-0.3,0.3,250) # x axis (lower limit, upper limit, sample size)
y = np.sum([norm.pdf(xvec,loc=0,scale=sig) for sig in e],axis=0)  # sum of probability distribution functions in each
y = y/np.sum(y)/(xvec[1]-xvec[0]) # 

z0 = np.mean(B)
z1 = np.std(B)

plt.figure()
plt.hist(B,bins=20,density=True) #histogram, normalised and with selected number of bins
plt.plot(xvec,y) # uncertainty
plt.plot(xvec,norm.pdf(xvec,loc=z0,scale=z1)) # gaussian distribution
plt.plot(xvec,kernel(xvec)) # kernal distribution
#plt.xscale('log')
plt.savefig('/data/public_html/mcginty/bayes_hist.png') # save histogram as png file in selected location

# simulate many Bayes-Factors from KDE
rmat = np.array([np.cumsum(kernel.resample(1000)) for _ in xrange(10)]) # produces 'y' cumulative Bayes Factors in 'xrange(y)' using 'x' samples each in 'resample(x)'
plt.figure()
for r in rmat:
    plt.plot(r) 
plt.savefig('/data/public_html/mcginty/sim_bayes.png') # save cumulative bayes factors as png file in selected location

exit(0)
