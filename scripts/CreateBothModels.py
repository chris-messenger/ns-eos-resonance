import numpy as np
#import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
import lal #LIGO Algorithm library
import lalsimulation #Inspiral Simulation Packages
from fractions import Fraction
import cmath #complex numbers
# from scipy.signal import tukey #allows us to cut off the ends of signals to produce one with the same start and end value. In this context, since the end is so valuable, we'll be cutting off the beginning.
#from scipy.ndimage import fourier
from numpy import interp
from scipy.optimize import brentq #returns solution within a given interval

# define fixed parameters
approximant = lalsimulation.IMRPhenomD
fs = 4096. #sampling freq
tobs = 1. #observation time
df = 1./tobs 
dt = 1./fs
fhigh = fs/2. #Nyquist frequency (Upper limit)

EquationsOfState = {}
#creating a dictionary containing all of the relevent info for equations of state from the Tsang et al. paper
Name = ['SLy4', 'APR', 'SkI6', 'SkO', 'Rs', 'Gs']
energymin = [5e46, 2e46, 3e45, 1e46, 1e46, 1e46]
tidalrate = [1e50, 9e49, 1e48, 1e49, 3e48, 3e48]
fmode = [188., 170., 67.3, 69.1, 32., 28.8]
for i in range(0,6):
    EquationsOfState[Name[i]] = [energymin[i], tidalrate[i], fmode[i]]
    

def get_fmin(M,eta,dt):
        #Compute the instantaneous frequency given a time till merger
        M_SI = M*lal.MSUN_SI

        def dtchirp(f):
        # The chirp time to 2nd PN order
            v = ((lal.G_SI/lal.C_SI**3)*M_SI*np.pi*f)**(1.0/3.0)
            temp = (v**(-8.0) + ((743.0/252.0) + 11.0*eta/3.0)*v**(-6.0) - (32*np.pi/5.0)*v**(-5.0) + ((3058673.0/508032.0) + 5429*eta/504.0 +(617.0/72.0)*eta**2)*v**(-4.0))
            return (5.0/(256.0*eta))*(lal.G_SI/lal.C_SI**3)*M_SI*temp - dt

        #solve for the frequency between limits
        fmin = brentq(dtchirp, 1.0, 2000.0, xtol=1e-6)

        return fmin


def CreateModelNoShatter(ModelKey,phi,mass1,mass2):
    

    energymin = EquationsOfState[ModelKey][0]
    tidalrate = EquationsOfState[ModelKey][1]
    fmode = EquationsOfState[ModelKey][2]
    #fixed parameters                                                                                                                                                                
    #mass1 = 1.4 #solar masses                                                                                                                                                          #mass2 
    m1 = mass1*lal.MSUN_SI
    m2 = mass2*lal.MSUN_SI
    approximant = lalsimulation.IMRPhenomD
    distance = 200e6*lal.PC_SI
    fs = 4096. #sampling freq                                                                                                                                                        
    tobs = 1. #observation time                                                                                                                                                      
    df = 1/tobs 
    fhigh = fs/2.
    M = mass1 + mass2
    eta = mass1*mass2/M**2
    flow = get_fmin(M,eta,tobs) 
    #flow = 134.354387713
    fref = flow

    
    #create model 1 waveform (no shattering)
    hp,hc = lalsimulation.SimInspiralChooseFDWaveform(m1,m2,0,0,0,0,0,0,distance,0,0,0,0,0,df,flow,fhigh,fref,lal.CreateDict(),approximant)
    hp = hp.data.data #positive polarisation
    hc = hc.data.data #cross polarisation

    hp = hp*cmath.exp(0+phi*1j)
    return (hp)

def CreateModelShatter(ModelKey,phi,mass1,mass2):
    """
    input EOS and return the waveform for both model 1 and model 2
    """
    def get_fmin(M,eta,dt):
        """                                                                                                                                                                              
        Compute the instantaneous frequency given a time till merger                                                                                                                     
        """
        M_SI = M*lal.MSUN_SI

        def dtchirp(f):
            """                                                                                                                                                                          
            The chirp time to 2nd PN order                                                                                                                                               
            """
            v = ((lal.G_SI/lal.C_SI**3)*M_SI*np.pi*f)**(1.0/3.0)
            temp = (v**(-8.0) + ((743.0/252.0) + 11.0*eta/3.0)*v**(-6.0) - (32*np.pi/5.0)*v**(-5.0) + ((3058673.0/508032.0) + 5429*eta/504.0 +(617.0/72.0)*eta**2)*v**(-4.0))
            return (5.0/(256.0*eta))*(lal.G_SI/lal.C_SI**3)*M_SI*temp - dt

        # solve for the frequency between limits                                                                                                                                         
        fmin = brentq(dtchirp, 1.0, 2000.0, xtol=1e-6)

        return fmin

    energymin = EquationsOfState[ModelKey][0]
    tidalrate = EquationsOfState[ModelKey][1]
    fmode = EquationsOfState[ModelKey][2]
    
    
    def find_nearest(array,value):
        idx = (np.abs(array-value)).argmin()
        return idx
    
    def get_instphase(x,dt):
        """
        function that returns the integrated phase of a psuedo-sinusoidal
        timeseries
        """
        # find zero crossing indices of timeseries
        idx = np.argwhere(np.diff(np.sign(x))!=0).squeeze()

        # initialise some parameters
        N = x.size
        r = []

        # loop over zero crossing indices
        # each crossing is a half cycle step = pi radians
        for i in idx:
            # check if we're too close to the start or end
            if i>0 and i<N-3:
                # interpolate over 4 points spanning the crossing
                # find the true crossing point in time
                y = interp1d(x[i-1:i+3],np.arange(-1,3),kind='cubic')
                r.append((i-1+y(0))*dt)

        # return a function that interpolates the cumulative phase (pi*halfcycles)
        # for a given time value
        r = np.array(r)
        return interp1d(r,np.pi*np.arange(r.size),kind='cubic',fill_value='extrapolate')
    
    def phase_jump(crackpos, shatterpos, deltaphase, t, phase):
        """
        function to input the jump in the phase due to the shattering
        """

        phasethree = phase[shatterpos:]+deltaphase
        phaseone = phase[0:crackpos+1]
        
        timenew = np.append(np.array(t[0:crackpos+1]),(np.array(t[shatterpos:])))
        phasejump = np.append(phaseone,phasethree)
        timebetween = np.array(t[crackpos+1:shatterpos])
            
        phasetwot = interp1d(timenew,phasejump,kind='quadratic')
        phasetwo = phasetwot(timebetween)
        phasetwo = np.array(phasetwo)
    
        phase12 = np.append(phaseone, phasetwo)
        return np.append(phase12,phasethree)
    
    def wave_return(phase_est,phasenew,t,x,shatterpos,crackpos):
        newphaseshatter = phasenew[shatterpos]
        phase = phase_est(t)
        #plt.figure()
        #plt.plot(t,phasenew)
        #plt.plot(t[shatterpos],phasenew[shatterpos],'x')
        #plt.show()
        
        interpphaseesttime = interp1d(phase[shatterpos:shatterpos+30],t[shatterpos:shatterpos+30],kind='quadratic')
        timeonoldphase = interpphaseesttime(newphaseshatter)
        
        timechange = timeonoldphase -t[shatterpos]
        
        #0 to cracking - waveform should be the same
        x1 = x[0:crackpos]
        t1 = t[0:crackpos]
        #shatter to end - phase change
        t3 = t[shatterpos:]+timechange #change time at shatter
        x3 = x[shatterpos:]
        #interpolate in the middle
        x2interp = interp1d(np.append(t1,t3),np.append(x1,x3),kind='quadratic')
        x2 = x2interp(t[crackpos:shatterpos])
        t2 = t[crackpos:shatterpos]
        x12 = np.append(x1,x2)
        newwaveform = np.append(x12,x3)
        t12 = np.append(t1,t2)
        newtimewaveform = np.append(t12,t3)
        return (newwaveform,newtimewaveform)
    
    
        #fixed parameters
    #mass1 = 1.4 #solar masses
    #mass2 = 1.4
    m1 = mass1*lal.MSUN_SI
    m2 = mass2*lal.MSUN_SI
    approximant = lalsimulation.IMRPhenomD 
    distance = 200e6*lal.PC_SI
    fs = 4096. #sampling freq
    tobs = 1. #observation time
    df = 1./tobs
    dt = 1./fs
    fhigh = fs/2.
    M = mass1 + mass2
    eta = mass1*mass2/M**2
    flow = get_fmin(M,eta,tobs)
    #flow = 134.354387713
    fref = flow
    masschirpfrac1 = Fraction('3/5')
    masschirpfrac2 = Fraction('1/5')
    masschirp = ((mass1*mass2)**masschirpfrac1)/((mass1+mass2)**masschirpfrac2)
    
    #Shattering parameters derived from the equation of state given
    epsilonsq = energymin / 5e46
    energyelastic = 2e46*epsilonsq
    durationofcrack = energyelastic / tidalrate
    timefrac1 = Fraction('5/3')
    timefrac2 = Fraction('8/3')
    inspiraltimescale = ((4.7e-3)*((1.2)**timefrac1)*((1e3)**timefrac2))/(((masschirp)**timefrac1)*((fmode)**timefrac2))
    timebeforecoal = 3.*inspiraltimescale/8.
    
    fracphi1 = Fraction('1/3')
    fracphi2 = Fraction('2/3')
    deltaphi = 0.001 #0.00018*((100/fmode)**fracphi1)*((m1/m2)**fracphi1)*(((m1+m2)/(2.8*lal.MSUN_SI))**fracphi2)*((((1.4*lal.MSUN_SI)**2)/(m1*m2))**2)#0.001 #1e-3 #radians #not sure how to get this from the EOS
    alpha = 1.0/32.0
    
    freqfrac1 = Fraction('3/8')
    freqfrac2 = Fraction('5/8')
    timeshatter = 8.*(timebeforecoal - durationofcrack)/3.
    freqshatter = 1e3*((4.7e-3/timeshatter)**freqfrac1)*((1.2/masschirp)**freqfrac2)
    
    #create model 1 waveform (no shattering)
    hp,hc = lalsimulation.SimInspiralChooseFDWaveform(m1,m2,0,0,0,0,0,0,distance,0,0,0,0,0,df,flow,fhigh,fref,lal.CreateDict(),approximant)
    hp = hp.data.data
    hc = hc.data.data
    xax = np.arange(0,len(hp))
    freqwave = np.array(xax*df)
    
    hp = hp*cmath.exp(0+phi*1j)
    
    newphaseshatter = 1.
    phaseshatterend = 0.
    newphaseshatterlater = 0.
    newphasebefore = 2.
    while newphaseshatter >= newphaseshatterlater or newphaseshatter >= phaseshatterend or newphasebefore >= newphaseshatter:
        temp = tukey(len(hp)-int(flow/df),alpha)
        window = np.zeros(len(hp))
        window[int(flow/df):]=temp
        newthing = hp*window#-np.mean(hp*window)
        timehp = np.fft.irfft(newthing)
        xaxt = np.arange(0,len(timehp))
        timewave = np.array(xaxt*dt)
    
        timeforcrack = tobs - timebeforecoal
        indexcrack = find_nearest(timewave,timeforcrack)
        timeforshatter = timeforcrack + durationofcrack
        indexshatter = find_nearest(timewave, timeforshatter)
        if indexcrack == indexshatter:
            indexshatter = indexshatter +1
        
    
        phase_est = get_instphase(timehp,dt)
    
    
    
        phasenew = phase_jump(indexcrack,indexshatter,deltaphi,timewave,phase_est(timewave))
    

        newphaseshatter = phasenew[indexshatter]
        newphaseshatterlater = phasenew[indexshatter+1]
        phaseshatterend = phasenew[indexshatter+80]
        newphasebefore = phasenew[indexshatter-1]
        
    
        alpha = alpha - 0.01
   
    
    newstrain = wave_return(phase_est,phasenew,timewave,timehp,indexshatter,indexcrack)
    
    nonshift = newstrain[0][0:indexcrack]
    timeinterp = timewave[indexcrack:]
    newtd = interp1d(newstrain[1],newstrain[0],kind='quadratic',bounds_error=False,fill_value = 0)
    shiftedstrain = np.append(nonshift,newtd(timeinterp))
    
    
    indexfornozero = np.where(window != 0)
    fourierfr = np.fft.rfft(shiftedstrain)#+np.mean(hp*window)
    fourierfrhp = np.fft.rfft(timehp)
    fourierfr[indexfornozero] = fourierfr[indexfornozero]/window[indexfornozero] 
    fourierfrhp[indexfornozero] = fourierfrhp[indexfornozero]/window[indexfornozero]
   
    return(fourierfr)

def dtchirp(f,m_si,eta):
    # The chirp time to 2nd PN order
    v = ((lal.G_SI/lal.C_SI**3)*m_si*np.pi*f)**(1.0/3.0)
    temp = (v**(-8.0) + ((743.0/252.0) + 11.0*eta/3.0)*v**(-6.0) - (32*np.pi/5.0)*v**(-5.0) + ((3058673.0/508032.0) + 5429*eta/504.0 +(617.0/72.0)*eta**2)*v**(-4.0))
    return (5.0/(256.0*eta))*(lal.G_SI/lal.C_SI**3)*m_si*temp

def get_fmin_FAST(M,eta,dt):
    # Compute the instantaneous frequency given a time till merger
    m_si = M*lal.MSUN_SI

    def dtchirp_min(f):
        # The chirp time to 2nd PN order
        return dtchirp(f,m_si,eta) - dt

    # solve for the frequency between limits
    fmin = brentq(dtchirp_min, 1.0, 2000.0, xtol=1e-6)

    return fmin

# def dec_to_rad(dec_string): #not necessary for our purposes
  # dec_to_rad(dec_string):
     # Given a string containing DEC information as'dd:mm:ss.ssss', return the equivalent decimal radians. Also deal with cases where input string is just dd:mm or dd
  # dms = dec_string.split(":")
  # if "-" in dms[0] and float(dms[0]) == 0.0:
    # m = '-'
  # else:
    # m = ''

  # if len(dms) == 3:
    # return dms_to_rad(int(dms[0]), int(m+dms[1]), float(m+dms[2]))
  # elif len(dms) == 2:
    # return dms_to_rad(int(dms[0]), int(m+dms[1]), 0.0)
  # elif len(dms) == 1:
    # return dms_to_rad(float(dms[0]), 0.0, 0.0)
  # else:
    # print("Problem parsing DEC string %s" % dec_string, file=sys.stderr)
    # sys.exit(1)

def antenna_response(det, ra, dec, psi, gpsTime):
    # input sky position and polarisation angle to return antenna response.

    # create detector-name map
    detMap = {'H1': lal.LALDetectorIndexLHODIFF, \
            'H2': lal.LALDetectorIndexLHODIFF, \
            'L1': lal.LALDetectorIndexLLODIFF, \
            'G1': lal.LALDetectorIndexGEO600DIFF, \
            'V1': lal.LALDetectorIndexVIRGODIFF, \
            'T1': lal.LALDetectorIndexTAMA300DIFF, \
            'AL1': lal.LALDetectorIndexLLODIFF, \
            'AH1': lal.LALDetectorIndexLHODIFF, \
            'AV1': lal.LALDetectorIndexVIRGODIFF, \
            'E1': lal.LALDetectorIndexE1DIFF, \
            'E2': lal.LALDetectorIndexE2DIFF, \
            'E3': lal.LALDetectorIndexE3DIFF}

    try:
      detector=detMap[det]
    except KeyError:
      raise ValueError, "ERROR. Key %s is not a valid detector name." % (det)

    # get detector
    detval = lal.CachedDetectors[detector]
    response = detval.response

    # check if ra and dec are floats or strings (if strings in hh/dd:mm:ss.s format then convert to rads)
    # not necessary for our purposes
    # if not isinstance(ra, float):
      # if isinstance(ra, basestring):
        # try:
          # ra = ra_to_rad(ra)
        # except:
          # raise ValueError, "Right ascension string '%s' not formatted properly" % ra
      # else:
        # raise ValueError, "Right ascension must be a 'float' in radians or a string of the format 'hh:mm:ss.s'"

    # if not isinstance(dec, float):
      # if isinstance(dec, basestring):
        # try:
          # dec = dec_to_rad(dec)
        # except:
          # raise ValueError, "Declination string '%s' not formatted properly" % ra
      # else:
        # raise ValueError, "Declination must be a 'float' in radians or a string of the format 'dd:mm:ss.s'"

    # check if gpsTime is just a float or int, and if so convert into an array
    if isinstance(gpsTime, float) or isinstance(gpsTime, int):
      gpsTime = np.array([gpsTime])
    else: # make sure it's a numpy array
      gpsTime = np.copy(gpsTime)

    # if gpsTime is a list of regularly spaced values then use ComputeDetAMResponseSeries
    if len(gpsTime) == 1 or np.unique(np.diff(gpsTime)).size == 1:
      gpsStart = lal.LIGOTimeGPS( gpsTime[0] )
      dt = 0.
      if len(gpsTime) > 1:
        dt = gpsTime[1]-gpsTime[0]
      fp, fc = lal.ComputeDetAMResponseSeries(response, ra, dec, psi, gpsStart, dt, len(gpsTime))

      # return elements from Time Series
      return fp.data.data, fc.data.data
    else: # we'll have to work out the value at each point in the time series
      fp = np.zeros(len(gpsTime))
      fc = np.zeros(len(gpsTime))

      for i in range(len(gpsTime)):
        gps = lal.LIGOTimeGPS( gpsTime[i] )
        gmst_rad = lal.GreenwichMeanSiderealTime(gps)

        # actual computation of antenna factors
        fp[i], fc[i] = lal.ComputeDetAMResponse(response, ra, dec, psi, gmst_rad)

    return fp, fc

def CreateModelShatter_FAST(ModelKey,phi,mass1,mass2,distance=None,shatter=True):
    # input EOS and return the waveform for both model 1 and model 2

    energymin = EquationsOfState[ModelKey][0] # drawing on relevant information for each model from dictionary
    tidalrate = EquationsOfState[ModelKey][1] # ...
    fmode = EquationsOfState[ModelKey][2] # ...
    
    # rescale input mass parameters
    m1 = mass1*lal.MSUN_SI # conversion to SI units
    m2 = mass2*lal.MSUN_SI # conversion to SI units
    M = mass1 + mass2 # total mass of system
    m_si = M*lal.MSUN_SI # conversion to SI units
    eta = mass1*mass2/M**2 # symmetric mass ratio
    flow = get_fmin_FAST(M,eta,tobs) # 
    fref = flow # redefining flow
    masschirp = ((mass1*mass2)**(3.0/5.0))/(M**(1.0/5.0))
    
    # Shattering parameters derived from the equation of state given
    epsilonsq = energymin / 5e46
    energyelastic = 2e46*epsilonsq
    #durationofcrack = energyelastic / tidalrate
    #inspiraltimescale = ((4.7e-3)*((1.2)**(5.0/3.0))*((1e3)**(8.0/3.0)))/(((masschirp)**(5.0/3.0))*((fmode)**(8.0/3.0)))
    freqshatter = fmode # mode frequency leading to shatter
    timeshatter = tobs - dtchirp(freqshatter,m_si,eta) # not used

    # the phase jump
    # deltaphi = 0.0001
    deltaphi = 0.00018*((100/fmode)**(1.0/3.0))*((m1/m2)**(1.0/3.0))*(((m1+m2)/(2.8*lal.MSUN_SI))**(2.0/3.0))*((((1.4*lal.MSUN_SI)**2)/(m1*m2))**2) #phase shift due to shattering. ~10^-4 radians
    deltatime = deltaphi/(2.0*np.pi*freqshatter)

    #create model 1 waveform (no shattering)
    hp,hc = lalsimulation.SimInspiralChooseFDWaveform(m1,m2,0.0,0.0,0.0,0.0,0.0,0.0,distance,0.0,0.0,0.0,0.0,0.0,df,flow,fhigh,fref,lal.CreateDict(),approximant)
    hp = hp.data.data*cmath.exp(0+phi*1j)
    hc = hc.data.data*cmath.exp(0+phi*1j)
    freqwave = df*np.arange(len(hp))

    # apply freq-dependent phase shift
    if shatter==True:
        freqindex = int(freqshatter/df)
        hp[freqindex:] *= np.exp(2j*np.pi*freqwave[freqindex:]*deltatime)
        hc[freqindex:] *= np.exp(2j*np.pi*freqwave[freqindex:]*deltatime)

    return hp, hc, freqwave
