import unittest
import numpy as np
import cpnest.model
import random
import CreateBothModels
#import matplotlib.pyplot as plt

class ShatteringModel(cpnest.model.Model):
    def __init__(self,shattering=False):
        self.shattering=shattering
    names = ['phiref','mass1','mass2']
    bounds = [[0,2*np.pi],[0.5,4],[0.5,4]]
    tobs = 1.
    sigmasq = (tobs/4.)*((4e-24)**2)
    m1 = 0
    m2 = 0
    while m1<=0.5 or m1>=4:
        m1 = np.random.normal(1.35,0.1)
    while m2<=0.5 or m2>=4:
        m2 = np.random.normal(1.35,0.1)
    #xmod = CreateBothModels.CreateModelShatter('APR',1.748729843)
    xmod = CreateBothModels.CreateModelShatter('APR',np.random.uniform(0,2*np.pi),m1,m2)
    lengthmodel = len(xmod)
    #CreateBothModels.CreateModelShatter returns (original strain (Model1),new strain (Model2),frequency)
    noise = (np.random.normal(0,np.sqrt(sigmasq),lengthmodel)+(np.random.normal(0,np.sqrt(sigmasq),lengthmodel))*1j)
    data = xmod+noise
    
    
    
    #nested sampling for Model 2 (with shattering)
   
    def log_likelihood(self, x):
        #print("I am in the likelihood function!")
        if self.shattering:
            Modelinput = CreateBothModels.CreateModelShatter('APR', x['phiref'],x['mass1'],x['mass2'])
        else:
            Modelinput = CreateBothModels.CreateModelNoShatter('APR', x['phiref'],x['mass1'],x['mass2'])
        print(x['phiref'])
        
        logL = -((self.lengthmodel)*np.log(np.sqrt(2*np.pi))*np.log(np.sqrt(self.sigmasq)))-0.5*(1./self.sigmasq)*np.sum((np.absolute(self.data-Modelinput))**2)
        

        return logL
    
    #default prior is uniform prior within bounds
    def log_prior(self,p):
        if not self.in_bounds(p): return -np.inf

        logP = -np.log(2*np.pi)-(np.log(2*np.pi)*np.log(0.1))-(0.5*(1./(0.1)**2)*((np.absolute(p['mass1']-1.35)**2)))-(0.5*(1./(0.1)**2)*((np.absolute(p['mass2']-1.35)**2)))
        return logP
        

if __name__=='__main__':
    work=cpnest.CPNest(ShatteringModel(shattering=False),output='./noshatterfiles1',verbose=1,Nthreads=12,Nlive=1000,maxmcmc=1000)
    work.profile()

if __name__=='__main__':
    work=cpnest.CPNest(ShatteringModel(shattering=True),output='./shatterfiles/shatter1',verbose=1,Nthreads=12,Nlive=1000,maxmcmc=1000)
    work.run()

