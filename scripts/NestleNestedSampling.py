import numpy as np
from numpy import random 
from scipy.special import ndtri #ndtri returns the argument x for which the area under the Gaussian probability density function (integrated from minus infinity to x) is equal to y.
from scipy.interpolate import interp1d
import nestle #nested sampling module
import CreateBothModels #models examined
import time
import lal
import os
import sys
import six
# import matplotlib.pyplot as plt
from scipy.stats import norm, truncnorm, uniform

#Code for nesting sampling process using the Nestle algorithm. Marginalising over phiref, distance and both masses of the neutron stars.

AllBayes = [] # Empty list, to be filled later
rndidx = np.random.randint(1e9)
N = 250
for i in range(0,N): # Loop produces Bayes Factors up to the amount N in (0,N)
    tobs = 1. # time between beginning of observation and coaelescence
    asddata = np.genfromtxt('ET_D_data.csv', delimiter=',') # noise floor data
    freq = asddata[1:,0] #array of datapoints, assuming header
    asd = asddata[1:,1] #...
    noiseFloor = interp1d(freq, asd, kind='linear', bounds_error=False, fill_value=(asd[0],asd[-1])) # interpolation of noise floor data
    
    # distance = (400e6*lal.PC_SI/np.pi)*np.arccos(1.0-2.0*np.random.uniform(0,1,1))[0]
    phidata = np.random.uniform(0,2*np.pi) #draws samples randomly within given interval (uniform distribution)
    m1data = np.random.normal(1.35,0.1) #draws samples randomly within given interval (gaussian distribution)
    # m2data = np.random.normal(1.35,0.1) #draws samples randomly within given interval (gaussian distribution)
    a,b = (0-1.35)/0.1, (m1data-1.35)/0.1 # unit conversion from masses to standard deviations
    m2data = truncnorm.rvs(a,b, loc=1.35, scale=0.1) # produced to ensure m2 < m1
    assert np.all(m2data <= m1data) # ...

    dmax = 200 # maximum observation distance chosen
    cd = np.random.uniform(0.0001,1) #cumulative distribution value, with lower limit corresponding to likely closest neutron star binary (~10Mpc)
    # cd = 1.0 - random.random() # a technically more complete way of doing the above.
    ddata = 1e6*lal.PC_SI*dmax*(cd**(1.0/3.0)) # distance corresponding to cumulative distribution
    # ddata = 1e6*lal.PC_SI*np.random.normal(200/np.sqrt(3.0),10) #draws samples randomly within given interval (gaussian distribution)
    # mean = 200 # central value for distance normal distribution
    # sigma = 66 # standard deviation
    ra = 2*np.pi # right ascension (these values must be in radians)
    dec = np.pi/2.0 # declination
    psi = np.random.uniform(0,2*np.pi) # polarisation angle
    print 'phase = {}'.format(phidata) # for reference/checking
    print 'mass one = {}'.format(m1data) # ...
    print 'mass two = {}'.format(m2data) # ...
    print 'distance = {}'.format(ddata/(1e6*lal.PC_SI)) # distance in Mpcs for easier reference/ checking
    print 'right ascension = {}'.format(ra) # ...
    print 'declination = {}'.format(dec) # ...
    print 'polarisation angle = {}'.format(psi) # ...

    # def PosNormal(mean,sigma): #function 'cheats' with the normal distribution, returning one that only produces positive values
        # d_data = np.random.normal(mean,sigma) #draws samples randomly within given interval (gaussian distribution)
       #  return(d_data if d_data>=0 else PosNormal(mean,sigma))

    # d_data = PosNormal(mean,sigma) #redefining d_data outside PosNormal function
    # print 'd_data = {}'.format(d_data/(1e6*lal.PC_SI))
    #calling in-function values from CreateBothModels (positive and cross polarisation/ antenna response (plus frequency)), then calculating overall signal received
    hp, hc, freqwave = CreateBothModels.CreateModelShatter_FAST('APR',phidata,m1data,m2data,ddata,shatter=True)
    Name = ['E1', 'E2', 'E3', 'AL1', 'AH1', 'AV1', 'H1', 'H2', 'L1', 'G1', 'V1', 'T1']

    Sn = noiseFloor(freqwave) # define Sn = noiseFloor(frequency)
    sigmasq = (tobs/4.)*(Sn**2) # Variance
    invsigmasq = 1.0/sigmasq # Inverse - saves computational power to pre-calculate this    

    data = {}
    for j in range(3):
 
        fp, fc = CreateBothModels.antenna_response(Name[j], ra, dec, psi, 1e9)
        ht = (hp*fp)+(hc*fc)
        lengthmodel = len(ht) #length of signal. Used to ensure noise generated below is the length we want. This should be the same length for every value of ht we produce below.
        noise = (np.random.normal(0,np.sqrt(sigmasq/2.),lengthmodel)+(np.random.normal(0,np.sqrt(sigmasq/2.),lengthmodel))*1j) #noise (complex)
        data[Name[j]] = ht + noise
    #creating a list of antennae from which to loop for multiple antennae/ detectors
    
    #CreateBothModels.CreateModelShatter returns (original strain (Model1),new strain (Model2),frequency)
    #Original strain = Model 1 = No shatter
    #New strain = Model 2 = Shatter

    
    # print 'invsigmasq = ',invsigmasq

    def prior_transform(theta): # theta is a tuple containing the parameters
        phireff,massone,masstwo,distref,psii = theta #unpack the parameters 
    
        phiref = (2*np.pi)*phireff #constant
        #mass1 = 1.35+0.1*ndtri(massone) # assuming prior mass distribution is gaussian, mean mass = 1.35 sol mass; stand dev = 0.1
        #mass2 = 1.35+0.1*ndtri(masstwo) # ...
        mass1 = norm.ppf(massone, loc=1.35, scale=0.1) # mass of neutron star 1
        #mass2 = norm.ppf(masstwo, loc=1.35, scale=0.1) # mass of neutron star 2
        a,b = (0-1.35)/0.1, (m1data-1.35)/0.1 # unit conversion from masses to standard deviations
        mass2 = truncnorm.ppf(masstwo, a, b, loc=1.35, scale=0.1) # produced to ensure m2 < m1
        dmax = 200 # maximum distance from source to observer
        cd = 0.0001 + distref*(1.-0.0001) # cumulative distribution value
        
        distance = 1e6*lal.PC_SI*dmax*(cd**(1.0/3.0)) # corresponding distance
        # ra = 0 + 2*np.pi*raa
        # dec = (-np.pi/2.) + np.pi*decc
        psi = uniform.ppf(psii, 0, 2*np.pi) # # polarisation angle
        # print 'phiref, mass1, mass2, distance = ',phiref,mass1,mass2,distance
        return (phiref, mass1, mass2, distance, psi)

    def loglikelihood_nestle_shatter(theta):
        phiref,mass1,mass2,distance,psi = theta #unpack the parameters

        # print(phiref,mass1,mass2,distance,ra,dec,psi)

        hp, hc, _ = CreateBothModels.CreateModelShatter_FAST('APR', phiref, mass1, mass2, distance, shatter=True)
        logL = 0
        for i in range(0,3):
            fp, fc = CreateBothModels.antenna_response(Name[i], ra, dec, psi, 1e9)
            ht = (hp*fp)+(hc*fc)
            #data = ht+noise #self-explanitory
            #logL -= 0.5*np.sum((np.absolute((data-ht))**2)*invsigmasq)
            logL -= 0.5*np.sum((np.absolute((data[Name[i]]-ht))**2)*invsigmasq)       

# logL = -((lengthmodel)*np.log(np.sqrt(2*np.pi))*np.log(np.sqrt(sigmasq)))-0.5*(1./sigmasq)*np.sum((np.absolute(data-Modelinput))**2) # Taking the log of the probability reduces the time complexity and allows for clearer analysis
        
        # print 'logL = {}'.format(logL)
        return logL 
    # loglikelihood_nestle_shatter(theta)
    # logL = loglikelihood_nestle_shatter(theta)
       # print 'logL = ',logL

    def loglikelihood_nestle_noshatter(theta):
        # The log-likelihood function for model without shattering.
        phiref,mass1,mass2,distance,psi = theta #unpack the parameters       

        hp, hc, _ = CreateBothModels.CreateModelShatter_FAST('APR', phiref, mass1, mass2, distance,shatter=False)
        logL = 0
        for i in range(0,3):
            fp, fc = CreateBothModels.antenna_response(Name[i], ra, dec, psi, 1e9)
            ht = (hp*fp)+(hc*fc)
            # data = ht+noise #self-explanitory
            logL -= 0.5*np.sum((np.absolute((data[Name[i]]-ht))**2)*invsigmasq)  
        # logL = -0.5*np.sum((np.absolute((data-Modelinput))**2)*invsigmasq)
        # logL = -((lengthmodel)*np.log(np.sqrt(2*np.pi))*np.log(np.sqrt(sigmasq)))-0.5*(1./sigmasq)*np.sum((np.absolute(data-Modelinput))**2)

        # print 'logL = ',logL
        # print 'invsigmasq = ',invsigmasq
        return logL
    # theta = loglikelihood_nestle_noshatter(theta)
    # logL = loglikelihood_nestle_noshatter(theta)

    nlive = 1000 #number of live points
    method = 'multi' #use MultiNest algorithm
    ndims = 5 #seven parameters (phi, m1, m2, distance, psi)
    tol = 0.1 #tolerance, aka the stopping criterion. Loop ceases when this value is reached.
    print '{}: working on signal {}/{}'.format(time.asctime(),i,N)

    # nestle sampling process - integrates over the unknown parameters in the model (ie phiref, mass1, mass2)
    res_shatter = nestle.sample(loglikelihood_nestle_shatter, prior_transform, ndims, method=method, npoints=nlive, dlogz=tol, callback=nestle.print_progress)
    logZnestle_shatter = res_shatter.logz                                       # value of logZ
    infogainnestle_shatter = res_shatter.h                                      # value of the information gain in nats
    logZerrnestle_shatter = np.sqrt(infogainnestle_shatter/nlive)               # estimate of the statistcal uncertainty on logZ

    res_noshatter = nestle.sample(loglikelihood_nestle_noshatter, prior_transform, ndims, method=method, npoints=nlive, dlogz=tol, callback=nestle.print_progress)
    logZnestle_noshatter = res_noshatter.logz                                   # value of logZ
    infogainnestle_noshatter = res_noshatter.h                                  # value of the information gain in nats
    logZerrnestle_noshatter = np.sqrt(infogainnestle_noshatter/nlive)           # estimate of the statistcal uncertainty on logZ 

    print 'res_shatter = {}'.format(res_shatter) # for checking purposes
    print 'logZnestle_shatter = {}'.format(logZnestle_shatter) # ...
    print 'infogainnestle_shatter = {}'.format(infogainnestle_shatter) # ...
    print 'logZerrnestle_shatter = '.format(logZerrnestle_shatter) # ...
 
    BayesFactor = np.exp(logZnestle_shatter - logZnestle_noshatter) # division becoming subtraction under log rules
    AllBayes = np.append(AllBayes,BayesFactor) # Fills empty list (AllBayes) with generated Bayes Factors

    #Output this Bayes Factor
    #Prints generated Bayes Factor and relevant information as a text file for safety. Input this to plot_Bayes.py
    f = open("/data/public_html/mcginty/BayesOutput_"+str(rndidx)+".txt",'a')
    f.write('{} {} {} {} {} {} {} {} {}\n'.format(BayesFactor,np.sqrt(logZerrnestle_shatter**2 + logZerrnestle_noshatter**2),phidata,m1data,m2data,ddata/(1e6*lal.PC_SI),ra,dec,psi))
    f.close()

    # this section produces the posterior samples, either as a text file (g), or as corner plots ,for each N = i. The information in g can be used to produce corner plots at a later time. Comment or uncomment this as you see fit.
    nweights = res_shatter.weights/np.max(res_shatter.weights) # re-scale weights to have a maximum of one
    keepidx = np.where(np.random.rand(len(nweights)) < nweights)[0] # get the probability of keeping a sample from the weights
    postsamples = res_shatter.samples[keepidx,:] # get the posterior samples
    print('Number of posterior samples is {}'.format(postsamples.shape[0])) #show number of samples used

    g = np.savetxt('/data/public_html/mcginty/Samples_{}'.format(i),postsamples,delimiter=',') # saves samples as text files
    
    # try:
        # import matplotlib as mpl
        # mpl.use("Agg") # force Matplotlib backend to Agg
        # import corner # import corner.py
    # except ImportError:
        # sys.exit(1)

    # fig = corner.corner(postsamples, labels=[r"$phi$", r"$m1$", r"$m2$", r"$distance$", , r"$ra$", r"$dec$", r"$psi$"], truths=[phidata,m1data,m2data,ddata,psi])
    # fig.savefig('/data/public_html/mcginty/Nestle_{}.png'.format(i)) # produces a corner plot in order to more accurately compare/assess parameters
    
print('Finished')
