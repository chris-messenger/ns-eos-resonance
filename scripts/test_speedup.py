import numpy as np
import CreateBothModels as cbm
import matplotlib.pyplot as plt
import time
import lal

N = 100
t1 = time.time()
for _ in np.arange(N):
    x = cbm.CreateModelShatter('APR',1.0,1.4,1.4)
t2 = time.time()
old_tps = (t2-t1)/N
print 'OLD: time per signal = {} sec'.format(old_tps)

t1 = time.time()
for _ in np.arange(N):
    y = cbm.CreateModelShatter_FAST('APR',1.0,1.4,1.4,distance=200e6*lal.PC_SI,shatter=True)
t2 = time.time()
new_tps = (t2-t1)/N
print 'NEW: time per signal = {} sec'.format(new_tps)
print 'speed_up factor = {}'.format(old_tps/new_tps)


exit(0)
